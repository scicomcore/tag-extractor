import csv
import click
import pathlib
import json
from loguru import logger

logger.add("extract_dates_{time}.log")

def get_tag_values(json_file, tags):
    with open(json_file, 'r') as f:
        data = json.load(f)
        out = []

        for tag in tags:
            v = data.get(tag)
            try:
                out.append(v['Value'])
            except:
                out.append(None)
    return out

def jsons(p):
    dcm_jsons = list(p.rglob('*/*.json'))
    return dcm_jsons


@click.command()
@click.argument('data-dir', type=click.Path(exists=True))
@click.argument('csv-filename')
@click.argument('tags', nargs=-1)
@click.option('--client-file', type=click.Path(exists=True))
def main(data_dir, csv_filename, tags, client_file):

    clients = None
    if client_file:
        with open(client_file, 'r') as f:
            clients = f.read().splitlines()

    path = pathlib.Path(data_dir)

    if clients:
        def client_iter():
            for c in clients:
                yield(path / c) 
    else:
        client_iter = path.iterdir

    table = [['client', 'file'] + list(tags)]
    for client in client_iter():
        dcm_jsons = jsons(client)

        for dcm_json in dcm_jsons:
            try:
                tag_values = get_tag_values(dcm_json, tags)
                table.append([client.name, dcm_json.name] + tag_values)
            except:
                logger.error(f'Failed to extract tags from {dcm_json} (possibly corrupt)')
                continue
            break

    with open(csv_filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(table)


if __name__ == '__main__':
    main()
