# What?

A Python script to extract a specific set of tags from every dicom-header json in the `DATA` directory of  `OMI-DB`.

You will need to pip install `loguru` and `click`, but honestly, just use the
provided `conda` environment:

```
conda env create -f env.yaml --prefix ./conda-env
conda activate ./conda-env
```
and then execute
```
python extractor.py <path/to/omidb/DATA> <output.json> <tags> --client-file <clients.txt>
```

You can provide a list of tags to extract. If the option `client-file` is not
present, the entire `DATA` directory will be parsed. For example, if `clients.txt` consists of

```
1
```

Then
 
```
python extractor.py  ~/adde/DATA output.json 00020001 0002002 --client-file clients.txt
```

will extract tags `0002001` and `0002002` from client 1 in the `~/adde/DATA` folder.

The result is stored in one monster json of the form:

```json
{
    "clientID": {
        "dicom_header_filename": {
            "tag": "value"
        }
    }
}
```

# Uses

Fine for extracting a few tags from a set of clients, but don't go mad!
