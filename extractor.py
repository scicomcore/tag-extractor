import click
import pathlib
import json
from loguru import logger

logger.add("extract_dates_{time}.log")

def get_tag_values(json_file, tags, flatten=True):
    with open(json_file) as f:
        data = json.load(f)
        out = {}
        for tag in tags:
            try:
                out[tag] = data.get(tag)['Value']
                if flatten:
                    if len(out[tag]) == 1:
                        out[tag] = out[tag][0]
            except:
                 out[tag] = None

    return out

def jsons(p):
    dcm_jsons = list(p.rglob('*/*.json'))
    return dcm_jsons


@click.command()
@click.argument('data-dir', type=click.Path(exists=True))
@click.argument('json-filename')
@click.argument('tags', nargs=-1)
@click.option('--client-file', type=click.Path(exists=True))
def main(data_dir, json_filename, tags, client_file):

    clients = None
    if client_file:
        with open(client_file, 'r') as f:
            clients = f.read().splitlines()

    path = pathlib.Path(data_dir)

    if clients:
        def client_iter():
            for c in clients:
                yield(path / c) 
    else:
        client_iter = path.iterdir

    out = {}
    for client in client_iter():
        dcm_jsons = jsons(client)

        out[client.name] = {}
        for dcm_json in dcm_jsons:
            try:
                out[client.name][dcm_json.name] = get_tag_values(dcm_json, tags)
            except:
                logger.error(f'Failed to extract tags from {dcm_json} (possibly corrupt)')
                continue

    with open(json_filename, 'w') as f:
        json.dump(out, f)


if __name__ == '__main__':
    main()
